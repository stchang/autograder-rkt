#lang racket
(require racket/set)
(require racket/match)

(require rackunit)
(require rackunit/private/format) ; display-test-result

(provide
  autograde-tests
  in-autograde-tests?
  (struct-out exercise)
  (struct-out batch)
  visibility?
  set-current-exercise-output!
  get-current-exercise-output
  batch->json)

(define (visibility? vis)
  (match vis
    ['visible #t]
    ['hidden #t]
    ['after-due-date #t]
    ['after-published #t]
    [else #f]))

;; hidden: test case will never be shown to students
;; after_due_date: test case will be shown after the assignment's due date has passed
;; after_published: test case will be shown only when the assignment is explicitly published from the "Review Grades" page
;; visible (default): test case will always be shown)
(define/contract (visibility->string vis)
  (-> visibility? string?)
  (match vis
    ['visible "visible"]
    ['hidden "hidden"]
    ['after-due-date "after_de_date"]
    ['after-published "after-published"]))

(struct exercise (name score max-score output visibility) #:transparent)
(define (exercise-set-output ex output)
  (struct-copy exercise ex [output output]))
(define (exercise<? x y) (string<? (exercise-name x) (exercise-name y)))
(define (exercise->json ex)
  (hash
    'score (exercise-score ex)
    'max_score (exercise-max-score ex)
    'name (exercise-name ex)
    'output (exercise-output ex)
    'visibility (visibility->string (exercise-visibility ex))))

(define (test->string result)
  (define os (open-output-string))
  (define oldos (current-output-port))
  (begin
    (parameterize ([current-error-port os])
      (display-test-result result))
    (get-output-string os)))


(struct batch (exercises execution-time) #:transparent)
(define (batch->json b)
  (hash
    'tests (map exercise->json (batch-exercises b))
    'execution_time (batch-execution-time b)))

(define (valid-weight? w)
  (or (real? w)
      (and (pair? w) (real? (car w)) (visibility? (cdr w)))))

(define current-exercise-output (make-parameter (void)))

;; Called to test whether we are running inside an exercise
(define (in-autograde-tests?)
  (box? (current-exercise-output)))

;; When being run inside an exercise
(define (get-current-exercise-output)
  (define b (current-exercise-output))
  (if (box? b)
    (unbox b)
    (error "Can only be called inside (autogarade-tests)")))

(define (set-current-exercise-output! output)
  (define b (current-exercise-output))
  (if (box? b)
    (set-box! (current-exercise-output) output)
    (error "Can only be called inside (autogarade-tests)")))

(define (autograde-tests weights test #:capture-output? [capture-output? #t])
  ; Build an exercise struct, automatically computes the current grade from a multiplier
  ; which is applied to the max-grade (obtained from the weights map)
  (define (make-exercise name multiplier [output ""] [visibility 'visible])
    (unless (hash-has-key? weights name)
      (error (format "Test case '~a' is missing a weight!" name)))
    (define entry (hash-ref weights name))
    (unless (valid-weight? entry)
      (error (format "Test case '~a' weight is ill-formed. Expecting either the weight (a real), or a pair of weight and visibility. Instead, got: ~a" name entry)))
    (define max-grade (if (pair? entry) (car entry) entry))
    (define curr-vis (if (pair? entry) (cdr entry) visibility))
    (exercise name (* multiplier max-grade) max-grade output curr-vis))
  ; Converts a test-result into an exercise
  (define (test-result->exercise result)
    (let ([name (test-result-test-case-name result)]
          [multiplier (if (test-success? result) 1.0 0.0)]
          [output (if capture-output? (test->string result) "")])
      (make-exercise name multiplier output)))
  ; Add results
  (begin
    (define start (current-seconds))
    ;; exs is a map from test-name into exercise
    (define exs
      (fold-test-results
        (lambda (p exs)
          ;; For each test result, we convert it to an exercise
          ;; We are careful to detect two test cases with the same label,
          ;; which is flagged as an error.
          (define output (car p))
          (define result (cdr p))
          ; Convert the test-result into an exercise, overriding the output
          ; if the user invoked set-current-exercise-output!
          (define ex
            (let ([ex (test-result->exercise result)])
              ; If there is any output, override field output, otherwise
              (if output
                (exercise-set-output ex output)
                ex)))
          (define name (exercise-name ex))
          ; Ensure that we have no duplicates
          (if (hash-has-key? exs name)
            (error (format "Two test-cases with the same key: ~a" name))
            (hash-set exs name ex)))
        ; We use a hash-table to ensure we are not missing any test case
        (hash)
        test
        ; We override the run-test-case to parameterize the output
        #:run
        (lambda (name action)
          (define out (box #f))
          (define res
            (parameterize ([current-exercise-output out])
              (run-test-case name action)))
          (cons (unbox out) res))))
    ;; Finally, we make sure we did not forget to implement a test case
    ;; (that is, a weight is defined but there is no test case for that name)
    (if (not (eq? (hash-count exs) (hash-count weights)))
      (error
        (format
          "A weight is declared, but no test-case is defined for these names: ~a"
          (set-subtract (hash-keys weights) (hash-keys exs))))
      (let ([elapsed (- (current-seconds) start)]
            ; We sort the list (by name) to ensure the results are deterministic
            [sorted (sort (hash-values exs) exercise<?)])
        (batch sorted elapsed)))))

(module+ test
  (define user-tests
    (test-suite
      "Examples"
      (test-case
        "Example 1"
        (check-true (in-autograde-tests?))
        (check-equal? 1 2))
          ;; -----------
      (test-case
        "Example 2"
        ; At any point we can use this function to set the current exercise output
        ; otherwise, the output is set
        (set-current-exercise-output! "foo")
        (set-current-exercise-output! "fox")
        (check-equal? 2 2))))

  (check-false (in-autograde-tests?))
  ; Happy path
  (check-not-exn (lambda () (autograde-tests (hash "Example 1" 2.0 "Example 2" 3.0) user-tests)))
  ; Happy path
  (check-not-exn (lambda () (autograde-tests (hash "Example 1" (cons 2.0 'visible) "Example 2" (cons 3.0 'hidden)) user-tests)))
  ; Invalid weight
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" (cons 2.0 #f) "Example 2" 3.0) user-tests)))
  ; Invalid weight
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" (list 2.0 'visible) "Example 2" 3.0) user-tests)))
  ; Invalid weight
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" (cons 2.0 'foo) "Example 2" 3.0) user-tests)))
  ; One example missing
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" 2.0) user-tests)))
  ; Both examples missing
  (check-exn exn:fail? (lambda () (autograde-tests (hash) user-tests)))
  ; One example too many
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" 2.0 "Example 2" 3.0 "Example 3" 4.0) user-tests)))
  ; One example too many and one missing
  (check-exn exn:fail? (lambda () (autograde-tests (hash "Example 1" 2.0 "Example 3" 4.0) user-tests)))
  ; Check the return value to see if it makes sense
  (define weights (hash "Example 1" 2.0 "Example 2" (cons 3.0 'after-due-date) ))
  (test-case
    "Happy path (capture output)"
    (let* ([result (autograde-tests weights user-tests)]
           [elems (batch-exercises result)])
      ; The exercises is a list
      (check-equal? 2 (length (batch-exercises result)))
      (let ([e1 (list-ref elems 0)] [e2 (list-ref elems 1)])
        (check-equal? e2 (exercise "Example 2" 3.0 3.0 "fox" 'after-due-date))
        (check-equal? "Example 1" (exercise-name e1))
        (check-equal? 0.0 (exercise-score e1))
        (check-equal? 2.0 (exercise-max-score e1))
        (check-equal? 'visible (exercise-visibility e1))
        ; We don't want to hardcode the output of Rackunit as it is bound to
        ; the line number where the test-case was defined, so we just check
        ; that the output is nonempty.
        (check-not-equal? "" (exercise-output e1)))))
  (test-case
    "Happy path (don't capture output)"
    (let* ([result (autograde-tests weights user-tests #:capture-output? #f)]
           [elems (batch-exercises result)])
      ; The exercises is a list
      (check-equal? 2 (length (batch-exercises result)))
      (let ([e1 (list-ref elems 0)] [e2 (list-ref elems 1)])
        (check-equal? e2 (exercise "Example 2" 3.0 3.0 "fox" 'after-due-date))
        (check-equal? "Example 1" (exercise-name e1))
        (check-equal? 0.0 (exercise-score e1))
        (check-equal? 2.0 (exercise-max-score e1))
        (check-equal? 'visible (exercise-visibility e1))
        ; Even though test failed, since output was not captured we have that
        ; output is empty
        (check-equal? "" (exercise-output e1))))))
